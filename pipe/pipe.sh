#!/bin/bash -l

source "$(dirname "$0")/common.sh"

[[ ! -z "$APP_LOCATION" ]] && Params="--app $APP_LOCATION "

[[ ! -z "$API_LOCATION" ]] && Params="${Params}--api $API_LOCATION "

[[ ! -z "$OUTPUT_LOCATION" ]] && Params="${Params}--outputLocation $OUTPUT_LOCATION "

cd /bin/staticsites/
./StaticSitesClient --deploymentaction 'upload' --deploymentProvider 'Bitbucket' --apiToken $API_TOKEN "$Params"