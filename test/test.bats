#!/usr/bin/env bats

setup() {
    DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-static-web-apps-deploy"}

    echo "Building image..."
    docker build -t ${DOCKER_IMAGE}:0.1.0 .

    APP_LOCATION="${APP_LOCATION}"
    API_LOCATION="${API_LOCATION}}"
    OUTPUT_LOCATION="${OUTPUT_LOCATION}"

}

teardown() {
    echo "Teardown happens after each test."
}

@test "Static Web app can be deployed to Azure app service" {


    echo "Run test"
    run docker run \
        -e APP_LOCATION="${APP_LOCATION}" \
        -e API_LOCATION="${API_LOCATION}" \
        -e OUTPUT_LOCATION="${OUTPUT_LOCATION}" \
        -e API_TOKEN="${deployment_token}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo ${output}
    [[ "${status}" == "0" ]]


    echo ${output}
    [[ "${status}" -eq 0 ]]
    
}


