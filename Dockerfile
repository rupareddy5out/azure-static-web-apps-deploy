FROM mcr.microsoft.com/appsvc/staticappsclient:stable

# Copies your code file from your action repository to the filesystem path `/` of the container
COPY pipe /
RUN chmod a+x /*.sh

# Code file to execute when the docker container starts up (`entrypoint.sh`)

ENTRYPOINT ["sh", "/pipe.sh"]